%!TEX root = rel-rw-journal.tex

This section explains how we compute the semantic similarity used in Eq.~\ref{eq:disambiguation} to incrementally solve the \NED problem. In essence, we compute the mutual information between two probability distributions, derived from random walks, which we call \emph{semantic signatures}. In Eq.~\ref{eq:disambiguation} we need signatures from single entities and from partial assignments consisting of many (previously disambiguated) entities. Both kinds of signatures are computed using random walks; the only difference between them is the initialization of the preference vector used in the walks. 


% Figure~\ref{fig:sem-sig} illustrates the idea of the semantic signature of entities and documents.
% \begin{figure}
%     \centering
%     \begin{tabular}{c|ccccccc}
%            & $e_1$ & $\ldots$ & $e_i$ & $\ldots$ & $e_j$  & $\ldots$ & $e_n$ \\\hline
%         $e_1$ & $w_{11}$ & $\ldots$   & $w_{1i}$ & $\ldots$  & $w_{1j}$ & $\ldots$  & $w_{1n}$        \\
%         $\vdots$ &  & & & & & &       \\
%         $e_i$ & $w_{i1}$ & $\ldots$ & $w_{ii}$ & $\ldots$ & $w_{ij}$ & $\ldots$ & $w_{in}$        \\
%         $\vdots$ &   &  &  &  &  &  &        \\
%         $e_j$ & $w_{j1}$ & $\ldots$  & $w_{ji}$ & $\ldots$ & $w_{jj}$ & $\ldots$ & $w_{jn}$        \\
%         $\vdots$ &  &  &  &  &  &  &        \\
%         $e_k$ & $w_{k1}$ & $\ldots$ & $w_{ki}$ & $\ldots$ & $w_{kj}$ & $\ldots$ & $w_{kn}$        \\\hline\hline
%         $d$ & $w_{d1}$ & $\ldots$ & $w_{di}$ & $\ldots$ & $w_{dj}$ & $\ldots$ & $w_{dn}$        \\
%     \end{tabular}
%     \caption{\small Semantic signatures of entities and documents.}
%     \label{fig:sem-sig}
% \end{figure}



\subsection{Disambiguation Graphs}
\label{sec:building_disambiguation_graphs}

Our KBs, built from Web-scale knowledge resources such as Wikipedia, are graphs whose nodes correspond to entities and whose edges connect pairs of entities that are semantically related. More precisely, we build a graph $\KB=(E, L)$ where $E$ consists of all individual articles in Wikipedia and a link $e1\rightarrow e2$ exists in $L$ if either: (1) the article of $e_1$ has an explicit hyperlink to $e_2$; or (2) there is an article with hyperlinks to $e_1$ and $e_2$ within a window~\cite{DBLP:conf/cikm/CaiZZW13} of 500 words. In order to compute the \emph{local} similarity (recall Eq.~\ref{eq:disambiguation}) we keep the entire article associated with each entity.

Our \KB is both large and dense, rendering the cost of performing random walks prohibitive. Moreover, starting from the principle that each document belongs to a single yet unknown \emph{topic}, and given that it mentions only a small number of entities, it is reasonable to assume that the space of entities relevant to disambiguate any given document is a small subgraph of the \KB. It is on this \emph{disambiguation graph} $G$ consisting of all candidate entities and their immediate neighbors (recall Fig.~\ref{fig:entity-graph}) that we perform the random walks.

\paragraph*{\textbf{Selecting Candidate Entities}}

Given a mention $m\in M$, we query an \emph{alias dictionary} to find the \KB entities that are known to be referred to as $m$. This dictionary is built from various resources in Wikipedia, including the anchor text in the Wikilinks~\cite{cucerzan07} and redirect and disambiguation pages.To keep the computational cost low, we proceed as follows. First, we rank candidates separately by: (1) $\PRIOR(m,e)$, the probability that the mention $m$ refers to entity $e$; and (2) $\CONTEXT(m,e)$, the context similarity between the text describing $e$ in the \KB and the section of the document containing $m$. Next, we take the top-$k$ candidates from each list (we used $k=10$ in the experiments reported here) and discard candidate entities that are not in the union of the two lists.

As mentioned, our disambiguation graph contains not only candidate entities but also their immediate neighbors. However, because our \KB is so dense, keeping all neighbors would result in a prohibitively large disambiguation graph. To further reduce the computation costs, we prune all non-candidate entities that are either (1) connected to a single candidate entity or (2) have degree below 200. (This threshold was chosen experimentally.) Candidate entities are never pruned regardless of their degree. Fig.~\ref{fig:graph_stats} in the experimental section~\ref{sec:experiment} shows statistics about the disambiguation graphs built in our experimental evaluation. As one can see, even after this aggressive pruning, our disambiguation graphs have several thousand nodes for each document.


\subsection{Obtaining Semantic Signatures}
\label{sec:signatures}

Let $G=(V,E)$ be a graph such that $|V| = n$. A random walk with restart is a stochastic process that repeatedly traverses the graph randomly. The starting point of these traversals is determined by an $n$-dimensional \emph{preference vector}. If repeated a sufficient number of times, this process results in an $n$-dimensional vector corresponding to the probability distribution of reaching each vertex in the graph. We call such vectors \emph{semantic signatures}, which we use to solve the \NED problem.

\paragraph*{\textbf{Random Walks}}

Let $T$ be the transition matrix of $G$, with $T_{ij}$ being the probability of reaching vertex $e_j$ from vertex $e_i$, computed as follows:
\begin{equation*}
    T_{ij} = \frac{w_{ij}}{\sum_{e_k\in \OUT(e_i)} w_{ik}}
\end{equation*}
in which $\OUT(e_i)$ is the set of entities directly reachable from $e_i$, and $w_{ij}$ is the weight of the edge between $e_i$ and $e_j$, defined as the co-occurrence count of the corresponding entities in the KB (more details see below).

Let $r^t$ be the probability distribution at iteration $t$, and $r_i^t$ be the value for vertex $e_i$, then $r_i^{t+1}$ is computed as follows:
\begin{equation}
    r_i^{t+1} = \sum_{e_j \in \IN(e_i)}r_j^t \cdot T_{ji}
\end{equation}
in which $\IN(e_i)$ is the set of entities linking to $e_i$.

As customary, we incorporate a random restart probability in the $n$-dimensional \emph{preference vector}. Formally, the random walk process can be described as:
\begin{equation}
r^{t+1} = \beta \times r^t \times T + (1-\beta) \times \vec{v}
\label{eqn:pagerank}
\end{equation}
\noindent where $\vec{v}$ is the preference vector, and $\sum v_i = 1$. We also follow the standard convention and set $\beta = 0.85$.


\begin{algorithm}[t]
    \begin{algorithmic}[1]
        \REQUIRE $M=\{m_1, m_2, \ldots, m_n\}, \KB=(E,L), A: M\rightarrow E$
        \ENSURE Document disambiguation vector $\vec{d}$
        \medskip

        \STATE let $n$ be the size of the disambiguation graph
        \STATE $\vec{d}=\vec{0}_{(n)}$

        \IF {$A \neq \emptyset$}
            \FOR {$m,e \in A$}
                \STATE $\vec{d}_e = 1$
            \ENDFOR
        \ELSE
            \FOR {$m \in M$}
                \FOR{$e \in \CANDIDATE(m)$}
                    \STATE $\vec{d}_e = \PRIOR(e,m) \cdot \TFIDF(m)$
                \ENDFOR
            \ENDFOR
        \ENDIF
    \STATE normalize \vec{d}
    \RETURN \vec{d}
    \end{algorithmic}
\caption{docVecInit}
\label{alg:docVecInit}
\end{algorithm}


\paragraph*{\textbf{Semantic Signature of an Entity}}
The semantic signature of an entity $e$ is obtained by setting the preference vector $\vec{v}$ with $\vec{v}_e = 1$, and $\vec{v}_{e'} = 0$, where $\ e'\neq e$. The resulting distribution can be interpreted as the semantic relatedness of every node in the disambiguation graph to $e$. 

\paragraph*{\textbf{Semantic Signature of an Assignment}}
The semantic signature of a (partial) assignment is the result of a random walk using a preference vector \vec{d} meant to contain the entities representing the \emph{topic} of the document. In the iterative \NED algorithm, this vector is computed from the partial assignment \AHAT and updated each time a new mention is disambiguated (and thus added to \AHAT). 

This formulation does not work when no mentions have been disambiguated yet (i.e., $\AHAT = \emptyset$), which may occur at the first step of the algorithm. In this case, we use \emph{all} candidate entities of all mentions to represent the document, weighting them proportionally to their prominence. The procedure is given in Alg.~\ref{alg:docVecInit}; here, $\PRIOR(m, e)$ is a corpus prior derived from Wikipedia capturing how often the mention $m$ is used to refer to entity $e$, while the \TFIDF score of each mention is pre-computed using the entire Wikipedia corpus, considering all entity aliases.

\subsection{Computing the Semantic Similarity}

There are several ways one can estimate the similarity of two semantic signatures (i.e., probability distributions) $P$ and $Q$. One standard way of doing so is to use The Kullback-Leibler (KL) divergence:
\begin{equation}
    D_{\mathit{KL}}(P\parallel Q) = \sum_i P_i\log \frac{P_i}{Q_i}
\end{equation}

In this work, we use Zero-KL Divergence~\cite{DBLP:conf/emnlp/HughesR07}, a better approximation of the KL divergence that handles the case when $Q_i$ is zero.
\begin{equation}
   \mathit{ZKL}_\gamma (P, Q) = \sum_iP_i \left\{ \begin{array}{lr}
            \log \frac{P_i}{Q_i} & Q_i \neq 0 \\ \gamma & Q_i = 0
            \end{array}\right.
\end{equation}
in which $\gamma$ is a real number coefficient. (Following the recommendation in~\cite{DBLP:conf/emnlp/HughesR07}, we set $\gamma=20$.) The semantic similarity used in Equation~\ref{eq:disambiguation}:
\begin{equation}
    \SEMANTIC(e, \AHAT) = \frac{1}{\mathit{ZKL}_\gamma (\mathit{\SEMSIG}(e), \mathit{\SEMSIG}(\vec{d}))}
\end{equation}
Above, \vec{d} is a vector computed from the partial assignment \AHAT, as explained in Alg.~\ref{alg:docVecInit}.
