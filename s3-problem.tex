%!TEX root = rel-rw-journal.tex

This section first describes \NEDfull as an optimization problem and then gives an overview of our solution based on using Random Walks to estimate semantic similarities (Section~\ref{sec:semantic}) and a greedy, iterative approximation algorithm (Section~\ref{sec:disambiguation}).

\subsection{The \NED Problem}

Let $d$ be a document with all mentions to named entities marked up through an NER process, and $\KB=(E,L)$ be a knowledge base represented as a graph whose nodes in $E$ correspond to real world entities and links in $L$ capture relationships among them. The task of \NED is to assign unique entity identifiers from $E$ to the mentions in $d$, whenever appropriate. \NIL is used for mentions outside of the \KB. 

More precisely:

\begin{definition}[\NEDfull]
Given a set of mentions $M=\{m_1, \ldots, m_m\}$ in a document $d$, and a knowledge base $\KB=(E,L)$, the \NED problem is to find an assignment $\ASSIGN: M \rightarrow E\cup \{\NIL\}.$
\end{definition}

A good assignment $\ASSIGN$ balances two forces: the \emph{local} similarity between a mention $m_i$ and its linked entity $e_j=A(m_i)$, and the \emph{global} coherence among all entities in the assignment. 

As usual, we define $\LOCAL(m_{i}, e_{j})$ as:
\begin{equation}
    \LOCAL(m_{i}, e_{j}) = \alpha\,\PRIOR(m_i, e_j) + (1-\alpha) \CONTEXT(m_i, e_j)
    \label{eq:local}
\end{equation}
where $\PRIOR(m_i, e_j)$ is a corpus prior probability that $e_j$ is the right entity for $m_i$, usually derived from alias dictionaries built from the \KB, and $\CONTEXT(m_i, e_j)$ is the similarity between \emph{local} features extracted from text (e.g., keywords) surrounding $m_i$ in the document and descriptions associated to $e_j$ in the \KB. 

Conversely, $\GLOBAL(\ASSIGN)$ captures the global coherence assumption that all entities in the assignment form a semantically coherent unit, as follows:
\begin{align}
	\GLOBAL(\ASSIGN) = \displaystyle \sum_{e \in \ASSIGN[M]}\SEMANTIC(e, A)
	\label{eq:global}
\end{align}
in which $\SEMANTIC(e, A)$ measures the semantic similarity between an entity $e$ and all others in the assignment $A$. Maximizing the sum in Eq.~\ref{eq:global} is consistent with the \emph{document coherence assumption}, in which one expects the input document to belong to a single \emph{topic} (e.g., sports) under which all entities in the assignment $A$ are related. In fact, this maximization is what allows the \emph{collective} disambiguation of mentions (e.g., disambiguating the mention Saban to Nick Saban in Ex.~\ref{example2} increases the odds of linking Miami to the Miami Dolphins).

Our notion of semantic coherence, rooted in Information Theory, corresponds to the mutual information between probability distributions obtained from the stationary distributions from two separate \emph{random walk} processes on the entity graph: one always restarting from the entity, and the other restarting from all entities used in the assignment. This is accomplished by appropriately defining random restarting probability to each entity in the preference vectors used in the walks accordingly.

Under the reasonable assumption that the local similarity is normalized, we can formulate \NED as a min-max optimization where the goal is to \emph{maximize} the global coherence while minimizing the \emph{loss} in local pairwise similarity by the assignment, which can be estimated as $|M| - \sum_{m_i\in M} \LOCAL(m_i,\ASSIGN(m_i))$. An equivalent and simpler formulation of the problem is to find an assignment $\ASSIGN^*$ that maximizes:
\begin{equation}
	\ASSIGN^{*} = \argmax_{\ASSIGN}\left(\GLOBAL(\ASSIGN) \cdot \sum_{m_i, e_j \in \ASSIGN}\LOCAL(m_i, e_j)\right)
	\label{eq:global_EL}
 \end{equation} 

Note that both global coherence and local similarity are normalized among all candidates of each mention.

% Starting from mentions in $M$, we connect each mention $m_i$ to all entities in its candidate set $cand(m_i)$ with weight $\LOCAL(m_i, e_j)$. Assuming that all candidate sets do not overlap, each entity $e_j$ in a candidate set $cand(m_i)$ is connected to all entities in other candidate sets with weight $\SEMANTIC(e_j,e_k)$. Entities in one candidate set are not connected to incorporate the constraint that only one entity is selected in each candidate set. In this graph, any $m$-clique~(a complete subgraph with $m$ nodes from the graph) will contains exactly one entity from each candidate set. Thus the optimization problem is actually to findi a maximum $m$-clique from a weighted graph, an NP-hard problem that can be reduced from the maximal clique problem~\cite{DBLP:books/fm/GareyJ79}.

\subsection{Iterative Walking \NED}
\label{sec:overview_wned}

As previously observed (see, e.g.,~\cite{Hoffart11}), the \NED problem is intimately connected with a number of NP-hard optimizations on graphs, including the maximum $m$-clique problem~\cite{DBLP:books/fm/GareyJ79}, from which a polynomial time reduction is not hard to construct. Thus we resort to an iterative heuristic solution, which works as follows.

We start with a disambiguation graph $G$ (recall Fig.~\ref{fig:entity-graph}) containing all candidate entities and their immediate neighbors. We order the mentions by their number of candidates (i.e., their degree of ambiguity), and iterate over them in increasing order, incrementally building the \emph{approximate} assignment one mention at a time. In iteration $i$, we assign to mention $m_i$ the entity $e$ maximizing
\begin{equation}
    \ASSIGN(m_i) = \argmax_{e \in \CANDIDATE(m_i)}(\SEMANTIC(e, \AHAT) \cdot \LOCAL(m_i, e))
	\label{eq:disambiguation}
\end{equation}

Here, $\SEMANTIC(e, \AHAT)$ is the mutual information between the probability distributions generated from random walks restarting from $e$ and from \emph{all entities} in the partial assignment up to step $i-1$. More precisely, let $n$ be the size of $G$. We define the \emph{semantic signature} of candidate entity $e \in G$ the $n$-dimensional vector corresponding to the stationary probability distribution of reaching each node in $G$ from $e$, obtained through a random walk that always restarts from $e$. The \emph{semantic signature} of the partial assignment \AHAT is also an $n$-dimensional vector with the stationary probability obtained from a random walk restarting from the entities corresponding to all mentions disambiguated up until that point. 

To disambiguate $n$ mentions, we re-compute \AHAT and its corresponding signature $n-1$ times. In the first step, we initialize \AHAT with (the entities of) all unambiguous mentions (inspired by~\cite{Milne08}). If all mentions are ambiguous to start with, we initialize \AHAT with all candidate entities weighted by their prior probability.


\paragraph*{\textbf{Linking to \NIL}} A mention is linked to \NIL in one of two cases: (1) when the mention has no good candidate entities; and (2) when the similarity score of the entity maximizing Eq.~\ref{eq:disambiguation} and the document is below a threshold. Both thresholds are, of course, application-specific. In future work, we will study their impact on typical EL tasks.

\subsection{Supervised Walking \NED}
\label{sec:overview_l2r}

\NED is the kind of problem suitable to supervised machine learning methods, as the assignment of entities to mentions is very hard to codify algorithmically. We developed a supervised algorithm that casts \NED as learning to rank the candidate entities for a mention. The features are the building blocks for computing the local and semantic similarities used by our iterative algorithm, including those derived from the random walks. Our algorithm uses Boosted Regression Trees, and returns the entity with the highest score among those predicted as a correct match. Of course, one drawback of this approach is the limited availability of training data; however, for the domain of news articles, we were able to develop a robust method trained on the CoNLL dataset which proved to be highly effective across the board in our experimentations on public benchmarks and the novel benchmarks we develop in this work.



