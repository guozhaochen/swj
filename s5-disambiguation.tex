%!TEX root = rel-rw-journal.tex
\newcommand{\INDSTATE}[1][1]{\hspace{#1\algorithmicindent}}

\begin{algorithm}[t]
    \begin{algorithmic}[1]
        \REQUIRE $M=\{m_1, m_2, \ldots, m_n\}$, $\KB=(E,L)$
        \ENSURE Assignment $\ASSIGN: M\rightarrow E\cup\{\NIL\}$
        \medskip

        \STATE $\AHAT = \emptyset$

        \FOR {$m_i \in M$ such that $|\CANDIDATE(m_i)| = 1$}
            \STATE $\AHAT(m_i) = \CANDIDATE(m_i)$
        \ENDFOR
        
        \medskip        

        \FOR {$m_i \in M$ sorted by increasing $|\ALIASES(m_i)|$}
            \STATE \vec{d} = docVecInit($M, \KB, \AHAT$)
            \STATE $\mathit{max} = 0$
            \FOR {$e_j \in \CANDIDATE(m_i)$}
                \STATE $P= \SEMSIG(e_j)$; $Q= \SEMSIG(\vec{d})$
                \STATE $\SEMANTIC(e_j,\AHAT) = \displaystyle \frac{1}{\mathit{ZKL_\gamma}(P,Q)}$
                \STATE $\SCORE(e_j) = \LOCAL(m_i,e_j) \times \SEMANTIC(e_j,\AHAT)$
                \IF {$\SCORE(e_j) > \mathit{max}$}
                    \STATE $e^* = e_j\ $; $\mathit{max} = \SCORE(e_j)$
                \ENDIF
            \ENDFOR

            \medskip
            
            \STATE $\AHAT(m_i) = e^*$
        \ENDFOR
        \medskip

        \FOR {$m, e \in \AHAT$}
            \IF {$\SCORE(e) < \theta$}
                \STATE $\AHAT(m) = \NIL$
            \ENDIF
        \ENDFOR
        \RETURN \AHAT
    \end{algorithmic}
\caption{Iterative Mention Disambiguation}
\label{alg:iterative}
\end{algorithm}


This section gives the details of our two random-walk-based \NED algorithms. 

\subsection{Iterative Disambiguation}

Alg.~\ref{alg:iterative} shows our iterative Walking \NED (\METHOD) algorithm. Intuitively, it builds a partial assignment \AHAT by disambiguating one mention in each iteration.

It starts (lines 1--4) by assigning all \emph{unambiguous} mentions, if any, to their respective entities. The main loop of the algorithm (lines 5--17) goes through the mentions sorted by increasing \emph{ambiguity}, defined as the number of entities that a mention can refer to\footnote{Note this number is usually much higher than the number of \emph{candidates} our algorithm considers, due to the pruning described in Sec.~\ref{sec:building_disambiguation_graphs}.}, computing the preference vector for the signature of the partial assignment (line 6) and greedily assigning to the current mention the candidate entity with highest combined score (lines 7--16).

In line 11, $\LOCAL(m,e)$ is computed as in Eq.~\ref{eq:local}, with $\alpha=0.8$ (tuned experimentally).

A final step of the algorithm is to assign \NIL to those mentions whose even the best candidate entity has a low score. The cut-off threshold $\theta$ is application-defined.

\subsection{Disambiguation via Learning to Rank}

The \NED problem can be cast as an entity ranking problem for which we rank the candidates based on a score function~(e.g. E.q~\ref{eq:disambiguation}), selecting the one with the highest rank. As is the case in most scenarios, the ranking is based on multiple criteria (e.g., prior probability, context similarity, semantic similarity, etc.) that may apply differently in different situations, making it hard or impossible to craft a concise algorithm that performs well in all cases. If training data is available, one can use sophisticated learning models to arrive at a more accurate ranking.

This Learning to Rank approach originates from Information Retrieval, where the methods can be divided into three groups~\cite{DBLP:series/synthesis/2011Li}. The \emph{pointwise} approaches consider query-document pairs as independent instances, and employ classification or regression to predict the scores of the documents (given the query) and rank them accordingly. As such, \emph{pointwise} methods are not trained on actual rankings. On the other hand, \emph{listwise} approaches are trained on the full ranking of all documents returned for a query. Since rankings can be hard to obtain, the \emph{pairwise} approaches take ordered pairs of documents in which one document ranks \emph{higher} than the other. 


Our Learning-to-Rank Walking \NED solution \break (\METHODL) is a \emph{pairwise} method based on the LambdaMART method that employs the MART (Multiple Additive Regression Trees) algorithm to learn  Boosted Regression Trees. It takes advantage of LambdaRank gradients to bypass the difficulty of handling non-smooth cost function introduced by most IR measures, and combines the cost function and IR metrics together to utilize the global ranking structure of documents. LambdaMART has been proven successful for solving real world ranking problems~\footnote{An ensemble of LambdaMART rankers won Track 1 of the 2010 Yahoo! Learning to Rank Challenge}.

\paragraph*{\textbf{Features}}
Although there are many useful features for ranking~\cite{Zheng10}, our main goal is to establish the robustness and utility of the semantic similarity for the \NED task, rather than performing exhaustive feature engineering at the risk of over-fitting. Thus we use four features, all of which are familiar in this research area: prior probability, context similarity, semantic relatedness, and \emph{name similarity} which is measured by the N-Gram distance~\cite{kondrak2005} between a mention and the canonical name of the entity.

More precisely, given a mention-entity pair $m-e$, we extract the following features: (1) prior probability $\PRIOR(m, e)$, (2) context similarity $\CONTEXT(m, e)$, (3) name similarity $\NAMESIM(m, e)$, and (4) semantic relatedness $\SEMANTIC(e, \vec{d})$ in which $\vec{d}$ is obtained by Alg.~\ref{alg:docVecInit} using the the initial \AHAT, computed as in lines~1--4 in Alg.~\ref{alg:iterative}.

\paragraph*{\textbf{Training data}} 
Using a \emph{pairwise} ranking method, for each mention we need \emph{ordered} pairs of entities $e_1, e_2$ such that $e_1$ is ranked \emph{higher} than $e_2$. Such pairs are easy to obtain when gold standard benchmarking data is available. Given a mention $m$ and its correct entity $e$ in the gold standard, we obtain several other candidate entities for $m$ and use them as the lower ranked entities.

In our experiments, we verify the system performance with two training approaches. First, we used the standard 10-fold cross-validation in which we use $1/5$ of the dataset for training and the rest for testing. Also, we experimented with using one dataset for training, while testing on other datasets. Both approaches worked well. In particular, we found that training on the fairly large CoNLL dataset led to a robust method that worked well on other benchmarks.


