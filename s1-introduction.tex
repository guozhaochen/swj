%!TEX root = rel-rw-journal.tex

Named entities are the persons, organizations, locations, etc. that are explicitly mentioned in text using proper nouns. \emph{Named Entity Recognition} (NER), the task of finding named entities in text and assigning them a semantic type corresponding to the kind of entity, is a key step towards algorithmic understanding of natural language text, especially in the context of the Web and social media where facts or properties about named entities are described in many documents. The NER task is often performed in conjunction with other text processing to \emph{resolve} pronouns and/or abbreviations in the text to the actual named entities they refer to, a task called co-reference resolution~\cite{jurafsky08}.

\emph{\NEDfull} (\NED), also known as \emph{Entity Linking}, is the task of linking the named entities mentioned in the text (explicitly or implicitly via a pronoun or abbreviation) to pre-existing objects in a Knowledge Base (KB) of interest, thus grounding them in a surrogate to a real world entity. \NED is key for Information Extraction (IE) and thus has many applications, such as: (1) expanding or correcting KBs with facts of entities extracted from text~\cite{DBLP:conf/acl/JiG11}; (2) semantic search~\cite{popov2004kim}, the emerging paradigm of Web search that combines Information Retrieval approaches over document corpora with KB-style query answering and reasoning; and (3) in Natural Language Question Answering~\cite{DBLP:conf/emnlp/YahyaBERTW12}.

Our work mainly focused on the \NED task. This article describes highly effective algorithms for solving this problem, assuming the input is a KB and a document where all mentions to named entities (explicit or implicit) have been identified.

\paragraph*{\textbf{Challenges}}

The inherent ambiguity of natural language makes \NED a hard problem, even to humans. Most real world entities can be referred to in many different ways (e.g., people have nicknames), while the same textual mention may refer to multiple real-world entities (e.g., different people have the same name). The following examples illustrate the issues:

\begin{example}
\label{example1}
\hfill \break
\underline{Saban}, previously a head coach of \underline{NFL}'s \underline{Miami}, is now coaching \underline{Crimson Tide}. His achievements include leading \underline{LSU} to the \underline{\smash{BCS National Championship}} once and \underline{Alabama} three times.
\end{example}

%After his departure from Buffalo[Buffalo Bills], Saban[Lou Saban] returned to coach college football teams including Miami[Miami Hurricanes football], Army[Army Black Knights football] and UCF[UCF Knights football].
\begin{example}
\label{example2}
\hfill \break
After his departure from \underline{Buffalo}, \underline{Saban} returned to coach college football teams including \underline{Miami}, \underline{\smash{Army}} and \underline{UCF}.
\end{example}

In Example~\ref{example1}, the mentions ``Crimson Tide'' and ``Alabama'' refer to the same football team, the \emph{Alabama Crimson Tide} of the University of Alabama, while the mention ``Saban'' refers to two different people (both football coaches): \emph{Nick Saban} in Example~\ref{example1} and \emph{Lou Saban} in Example~\ref{example2}. Similarly, ``Miami'' in Example~\ref{example1} refers to the NFL football team \emph{Miami Dolphins}, while in Example~\ref{example2} it refers to the college football team \emph{Miami Hurricanes}. 

\begin{figure*}
  \centering
  \includegraphics[scale=0.55]{figures/example}
  \caption{\small Example named entity disambiguation scenario.}
  \label{fig:entity-graph}
\end{figure*}

\subsection{Canonical Solution} 

The \NED task can be cast as an all-against-all matching problem: given $m$ mentions in a document and $n$ entities in a KB, perform the $m\times n$ comparisons and pick the ones with the highest similarity (one for each mention). This is prohibitively expensive and unnecessary, however. Most methods, including ours, solve this issue in two stages: the first stage reduces the search space by selecting a suitable set of candidate entities for each mention, and the second performs the actual mention disambiguation. Selecting candidate entities is done, primarily, by consulting alias dictionaries (see, e.g.,~\cite{Singh2012}). Candidate selection is meant to be fast, and thus leads to false positives which must be filtered out in the actual disambiguation phase. In both examples above, both coaches \emph{Lou Saban} and \emph{Nick Saban} would be picked as candidates for the mention ``Saban'', as would other kinds of entities, such as the organizations \emph{Saban Capital Group} and \emph{Saban Entertainment}. 

As for the disambiguation phase, the methods in the literature can be divided into two groups: local and global disambiguation methods, as discussed next.

\paragraph*{\textbf{Local Disambiguation}} The first group of \NED systems focused mainly on lexical features, and statistical features, such as contextual words surrounding the mentions in the document~\cite{Baldwin1998,bunescu06} or statistical probability from knowledge bases. Moreover, mentions are disambiguated independently, typically by ranking the entities according to the similarity between the context vector of the mention and the text describing the entities in the KB (e.g., \emph{keyphrases}). These approaches work best when the context is rich enough to uniquely identify the mentions or the linked entities are popular enough, which is not always the case. For instance, both ``Saban'' and ``Miami'' in the examples above are hard to disambiguate locally because \emph{Lou Saban} coached the \emph{Miami Hurricanes} while \emph{Nick Saban} coached the \emph{Miami Dolphins} and such a dependency cannot be enforced via local methods based on context similarity. In fact, these kinds of dependencies are the main motivation for the global disambiguation methods.

\paragraph*{\textbf{Global Disambiguation}} Most current approaches are based on the premise that the disambiguation of one mention should affect the disambiguation of the remaining mentions (e.g., disambiguating \emph{Saban} to \emph{Nick Saban} should increase the confidence for \emph{Miami} to be linked to the \emph{Miami Dolphins}). In general, the idea is to start with a \emph{disambiguation graph} containing all mentions in the document and all \emph{candidate entities} from the KB. In many global \NED systems, the disambiguation graph also contains the immediate neighbors of the candidates (Fig.~\ref{fig:entity-graph}). The actual disambiguation is done \emph{collectively} on all mentions at the same time~\cite{cucerzan07,Hoffart11,DBLP:conf/kdd/KulkarniSRC09,Ratinov11}, by finding an  embedded forest in the disambiguation graph in which each mention remains linked to just one of the candidates. The edges are weighted according to multiple criteria, including local (context) similarity, priors, and some notion of \emph{semantic} similarity, possibly computed from the graph itself. Finding such an assignment is NP-Hard~\cite{DBLP:conf/kdd/KulkarniSRC09} under reasonable assumptions, and all methods turn to approximate algorithms or heuristics. One common way to cope with the complexity is to use a greedy search, starting with mentions that are easy to disambiguate (e.g., have just one candidate entity)~\cite{Milne08}. 

The choice of semantic similarity determines the accuracy and cost of each method. One successful strategy~\cite{DBLP:conf/cikm/HoffartSNTW12} computes the set-similarity involving (multi-word) \emph{keyphrases} about the mentions and the entities, collected from the KB. This approach works best when the named entities in the document are mentioned in similar ways to those in the corpus from which the knowledge base is built (typically, Wikipedia). Another approach~\cite{Milne2008b} computes the set similarity between the neighbor entities directly connected in the KB. Doing so, however, ignores entities that are indirectly connected yet semantically related, making limited use of the KB graph. 

In previous work~\cite{DBLP:conf/cikm/GuoB14}, we introduced a method that used an information-theoretic notion of similarity based on stationary probability distributions resulting from  random walks~\cite{DBLP:conf/icdm/TongFP06} on the disambiguation graph, which led to consistently superior accuracy. This paper presents substantial extensions over that method.

\subsection{Our approach} 

Our \METHOD (Walking Named Entity Disambiguation) method is a greedy, global \NED algorithm based on a sound information-theoretic notion of semantic relatedness derived from random walks on carefully built disambiguation graphs~\cite{DBLP:conf/cikm/GuoB14}. We build specific disambiguation graphs for each document, thus adhering to the notion of global coherence assumption---that coherent entities form a dense subgraph. By virtue of using random walks, our notion of similarity leverages indirect connections between nodes in the disambiguation graph, and is thus less susceptible to false positives incurred by disproportionately high priors of head entities. As confirmed by our experiments, our approach outperforms the previous state-of-the-art, and excels in disambiguating mentions of less popular entities.

\paragraph*{\textbf{Contributions}} This article presents several significant extensions over our previous work.
\begin{itemize}
\item First, we revised and simplified our optimization goal, resulting in a more robust greedy iterative algorithm with fewer tuning parameters.
\item Second, we show how to tune our method with an off-the-shelf standard ``learning-to-rank'' approach. We apply this idea using an existing manually-annotated dataset (the CoNLL dataset widely used in the literature) leading to an algorithm that consistently outperforms all previous methods, across benchmarks and by a wide margin.
\item Third, we report on a deeper experimental evaluation than previous works in the area: (1) our analysis shows that previous benchmarks are ``easy'', in the sense that a simple baseline can correctly disambiguate most mentions; (2) we introduce two benchmarks from real Web corpora (Wikipedia and Clueweb 2012) with documents of increasing difficulty, which are also \emph{balanced} (i.e., they have the same number of documents in each difficulty class); finally, (3) we aggregate the per-document accuracy in a more meaningful way.
\end{itemize}

It is also worth noting that our statically hand-tuned algorithm also outperformed all previous methods and is quite competitive with the learning approach. These observations corroborate the superiority and robustness of using random walks for the \NED task. In summary, the algorithm and the evaluation methodology described in this article significantly push the state-of-the-art in this task.

