%!TEX TS-program = latex

\documentclass{iosart2c}

\usepackage[T1]{fontenc}
\usepackage{times}%
\usepackage{natbib}
%\usepackage[dvips]{hyperref}
%\usepackage{amsmath}
\usepackage{dcolumn}
%\usepackage{endnotes}
\usepackage{graphics}

\usepackage{enumitem,url}
\usepackage[cmex10]{amsmath}
\usepackage{algorithmic,algorithm}
\usepackage{combelow}
%\usepackage{cite}
\usepackage{xspace}
\usepackage{color}
\usepackage{balance}
\usepackage{array}
\usepackage[pdftex]{graphicx}
\usepackage{multirow}
\usepackage[normalem]{ulem}
\usepackage{subcaption}

\newcommand{\FIX}[1]{\textcolor{red}{[#1]}}

\def\NIL{\ifmmode\mathsf{NIL}\else\textsf{NIL}\fi\xspace}

\def\AIDABALANCED{\textsc{AIDA-CoNLL}$^\dagger$\xspace}
\def\AIDACONLL{\textsc{AIDA-CoNLL}\xspace}

\def\LTRSELF{\textsc{l2r-SELF}\xspace}
\def\LTRCONLL{\textsc{l2r-CoNLL}\xspace}

\def\METHOD{\textsc{WNED}\xspace}
\def\METHODL{\textsc{l2r.WNED}\xspace}
\def\BASEPRIOR{\textsc{Prior}\xspace}
\def\BASELOCAL{\textsc{Context}\xspace}
\def\AIDA{AIDA\xspace}
\def\MW{M\&W\xspace}
\def\CUCERZAN{Cucerzan\xspace}
\def\RI{RI\xspace}
\def\HAN{Han11\xspace}
\def\GLOW{GLOW\xspace}

\def\L2R{L2R\xspace}
\def\KB{KB\xspace}

\def\ACCURACY{\ifmmode\mathit{accuracy}\else\textit{accuracy}\fi\xspace}
\def\PRECISION{\ifmmode\mathit{precision}\else\textit{precision}\fi\xspace}
\def\RECALL{\ifmmode\mathit{recall}\else\textit{recall}\fi\xspace}
\def\F1{\ifmmode\mathit{F1}\else\textit{F1}\fi\xspace}
\def\TRUTH{\ifmmode\mathit{truth}\else\textit{truth}\fi\xspace}
\def\RESULT{\ifmmode\mathit{result}\else\textit{result}\fi\xspace}

\def\vec#1{\ifmmode\mathbf{#1}\else$\mathbf{#1}$\fi\xspace}
\def\RANDOMWALK{\mathit{ComputeSignature}}


\def\ASSIGN{\ifmmode\mathit{A}\else\textit{A}\fi\xspace}
\def\AHAT{\ifmmode\mathit{\hat{A}}\else$\AHAT$\fi\xspace}
\def\GLOBAL{\ifmmode\mathit{global}\else\textit{global}\fi\xspace}
\def\LOCAL{\ifmmode\mathit{local}\else\textit{local}\fi\xspace}
\def\CANDIDATE{\ifmmode\mathit{cand}\else\textit{cand}\fi\xspace}
\def\ALIASES{\ifmmode\mathit{ambiguity}\else\textit{ambiguity}\fi\xspace}
\def\SEMANTIC{\ifmmode\mathit{semantic}\else\textit{semantic}\fi\xspace}
\def\PRIOR{\ifmmode\mathit{prior}\else\textit{prior}\fi\xspace}
\def\TFIDF{\ifmmode\mathit{tf\-idf}\else$\mathit{tf\-idf}$\fi\xspace}
\def\CONTEXT{\ifmmode\mathit{context}\else\textit{context}\fi\xspace}
\def\NAMESIM{\ifmmode\mathit{nameSim}\else\textit{nameSim}\fi\xspace}
\def\COHERENCE{\ifmmode\mathit{cohr}\else\textit{cohr}\fi\xspace}
\def\PROB{\ifmmode\mathit{prob}\else\textit{prob}\fi\xspace}
\def\IMPORTANCE{\ifmmode\mathit{importance}\else\textit{importance}\fi\xspace}
\def\SCORE{\ifmmode\mathit{score}\else\textit{score}\fi\xspace}

\def\OUT{\ifmmode\mathit{Out}\else\textit{Out}\fi\xspace}
\def\IN{\ifmmode\mathit{In}\else\textit{In}\fi\xspace}
\def\SEMSIG{\ifmmode\mathit{signature}\else\textit{signature}\fi\xspace}

\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\DeclareMathOperator*{\argmax}{arg\,max}

\algsetup{indent=1em}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}


\def\NEDfull{Named Entity Disambiguation\xspace}
\def\NED{NED\xspace}

\newcolumntype{d}[1]{D{.}{.}{#1}}

\begin{document}
\begin{frontmatter}

\title{Robust Named Entity Disambiguation with Random Walks}
\runningtitle{Robust Named Entity Disambiguation with Random Walks}

\author{\fnms{Zhaochen} \snm{Guo}}
and
\author{\fnms{Denilson} \snm{Barbosa}\thanks{Corresponding author. E-mail: denilson@ualberta.ca}}
\runningauthor{Z. Guo et al.}
\address{Department of Computing Science, University of Alberta, Edmonton, AB, Canada.\\
E-mail: \{zhaochen, denilson\}@ualberta.ca}

\begin{abstract}
Named Entity Disambiguation is the task of assigning entities from a Knowledge Base to \emph{mentions} of such entities in a textual document. This article presents two novel approaches guided by a natural notion of \emph{semantic similarity} for the collective disambiguation of all entities mentioned in a document at the same time. We adopt a unified semantic representation for entities and documents---the probability distribution obtained from a random walk on a subgraph of the knowledge base---as well as lexical and statistical features commonly used for this task. The first approach is an iterative and greedy approximation, while the second is based on learning-to-rank. Our experimental evaluation uses well-known benchmarks as well as new ones introduced here. We justify the need for these new benchmarks, and show that both our methods outperform the previous state-of-the-art by a wide margin.
\end{abstract}

% Note that keywords are not normally used for peerreview papers.
\begin{keyword}
Named entities\sep entity linking\sep entity disambiguation\sep relatedness measure\sep random walk
\end{keyword}

\end{frontmatter}

%\IEEEraisesectionheading{\section{Introduction}\label{sec:introduction}}
\section{Introduction}
\label{sec:intro}
\input{s1-introduction}

\section{\NED with Random Walks}
\label{sec:problem}
\input{s3-problem}

\section{Semantic Similarity}
\label{sec:semantic}
\input{s4-semantic}

\section{Disambiguation Algorithms}
\label{sec:disambiguation}
\input{s5-disambiguation}

\section{Experimental Validation}
\label{sec:experiment}
\input{s6-experiment}

\section{Related Work}
\label{sec:relatedwork}
\input{s2-relatedwork}

\section{Conclusion}
We described a method for named entity disambiguation that combines lexical and statistical features with \emph{semantic} signatures derived from random walks over suitably designed disambiguation graphs. Our semantic representation uses more relevant entities from the knowledge base, thus reducing the effect of feature sparsity, and results in substantial accuracy gains. We described a hand-tuned greedy algorithm as well as one based on learning-to-rank. Both outperform the previous state-of-the-art by a wide margin. Moreover, we showed that our \METHODL algorithm trained on the standard \AIDACONLL corpus is quite robust across benchmarks.

Moreover, we demonstrated several shortcomings of the existing \NED benchmarks and described an effective way for deriving \emph{better} benchmarks and described two new such benchmarks based on web-scale annotated corpora (ClueWeb12 and Wikipedia). Our benchmark generation method can be tuned to produce ``harder'' or ``easier'' cases as desired. Overall, the benchmarks we describe complement the largest currently available public benchmark. Our experimental evaluation compared our methods against six leading competitors and two very strong baselines, revealing the superiority and robustness of our entity linking system in a variety of settings. Our method was particularly robust when disambiguating unpopular entities, making it a good candidate to address the ``long tail'' in Information Extraction.

\paragraph*{Future work} A number of opportunities for future work exist. Sec.~\ref{sec:qualitative_analysis} lists several ideas for algorithmic improvements that can lead to better \NED systems in the future. Also, while the new benchmarks described here can be used for both accuracy and scalability tests (as one can easily obtain large quantities of documents from ClueWeb12 and Wikipedia), further work is needed in helping the design and verification of ground-truths. 

Our algorithms require multiple random walk computations, making it time consuming if implemented naively (as in our current implementation). On a standard entry-level server, the average time to disambiguate a document in our benchmarks (usually with less than 100 mentions) is in the order of a few minutes. Therefore, designing proper system infrastructure with the appropriate indexes and/or parallel computing infrastructure to optimize these computations would be interesting. Moreover, other state-of-the-art systems perform other expensive operations as well, such as accessing the Web or querying large relational databases. Therefore, designing objective and fair benchmarks for comparing these different approaches in terms of both accuracy and performance would be of great value to the community.

% references section
\begin{thebibliography}{0}

\bibitem{DBLP:conf/naacl/AgirreAHKPS09}
E. Agirre, E. Alfonseca, K. Hall, J. Kravalova, M. Pa\cb{s}ca, and A. Soroa, \textit{A study on similarity and relatedness using distributional and wordnet-based approaches}, in \emph{HLT-NAACL}, 2009, pp. 19--27.

\bibitem{Baldwin1998}
A.~Bagga and B.~Baldwin, \textit{Entity-based cross-document coreferencing using the vector space model}, in \emph{COLING-ACL}, 1998, pp. 79--85.

\bibitem{bunescu06}
R.~C. Bunescu and M.~Pa\cb{s}ca, \textit{Using encyclopedic knowledge for named entity disambiguation}, in \emph{EACL}, 2006.

\bibitem{DBLP:conf/cikm/CaiZZW13}
Z.~Cai, K.~Zhao, K.~Q. Zhu, and H.~Wang, \textit{Wikification via link co-occurrence}, in \emph{CIKM}, 2013, pp. 1087--1096.

\bibitem{DBLP:conf/icml/CaoQLTL07}
Z.~Cao, T.~Qin, T.~Liu, M.~Tsai, and H.~Li, \textit{Learning to rank: from pairwise approach to listwise approach}, in \emph{Machine Learning, Proceedings of the Twenty-Fourth International Conference {(ICML} 2007), Corvallis, Oregon, USA, June 20-24, 2007}, 2007, pp. 129--136.

\bibitem{DBLP:conf/emnlp/ChengR13}
X.~Cheng and D.~Roth, \textit{Relational inference for wikification}, in \emph{EMNLP}, 2013, pp. 1787--1796.

\bibitem{cucerzan07}
S.~Cucerzan, \textit{Large-scale named entity disambiguation based on wikipedia data}, in \emph{EMNLP-CoNLL}, 2007, pp. 708--716.

\bibitem{Dredze10}
M.~Dredze, P.~McNamee, D.~Rao, A.~Gerber, and T.~Finin, \textit{Entity disambiguation for knowledge base population}, in \emph{COLING}, 2010, pp. 277--285.

\bibitem{DBLP:books/fm/GareyJ79}
M.~R. Garey and D.~S. Johnson, \textit{Computers and Intractability: A Guide to the Theory of NP-Completeness}. W. H. Freeman, 1979.

\bibitem{DBLP:conf/cikm/GuoB14}
Z.~Guo and D.~Barbosa, \textit{Robust entity linking via random walks}, in \emph{Proceedings of the 23rd {ACM} International Conference on Conference on Information and Knowledge Management, {CIKM} 2014, Shanghai, China, November 3-7, 2014}, 2014, pp. 499--508.

\bibitem{Han2012}
X.~Han and L.~Sun, \textit{An entity-topic model for entity linking}, in \emph{EMNLP-CoNLL}, 2012, pp. 105--115.

\bibitem{Han11}
X.~Han, L.~Sun, and J.~Zhao, \textit{Collective entity linking in web text: a graph-based method}, in \emph{SIGIR}, 2011, pp. 765--774.

\bibitem{DBLP:conf/cikm/HoffartSNTW12}
J.~Hoffart, S.~Seufert, D.~B. Nguyen, M.~Theobald, and G.~Weikum, \textit{Kore: keyphrase overlap relatedness for entity disambiguation}, in \emph{CIKM}, 2012, pp. 545--554.

\bibitem{Hoffart11}
J.~Hoffart, M.~A. Yosef, I.~Bordino, H.~F{\"u}rstenau, M.~Pinkal, M.~Spaniol,  B.~Taneva, S.~Thater, and G.~Weikum, \textit{Robust disambiguation of named entities in text}, in \emph{EMNLP}, 2011, pp. 782--792.

\bibitem{DBLP:conf/emnlp/HughesR07}
T.~Hughes and D.~Ramage, \textit{Lexical semantic relatedness with random graph walks}, in \emph{EMNLP-CoNLL}, 2007, pp. 581--589.

\bibitem{DBLP:conf/acl/JiG11}
H.~Ji and R.~Grishman, \textit{Knowledge base population: Successful approaches and challenges}, in \emph{ACL}, 2011, pp. 1148--1158.

\bibitem{jurafsky08}
D.~Jurafsky and J.~H. Martin, \textit{Speech and Language Processing}, 2nd~ed. Prentice Hall, 2008.

\bibitem{kondrak2005}
G.~Kondrak, \textit{N-gram similarity and distance}, in \emph{String Processing and Information Retrieval}, ser. Lecture Notes in Computer Science. Springer Berlin Heidelberg, 2005, vol. 3772, pp. 115--126.

\bibitem{DBLP:conf/kdd/KulkarniSRC09}
S.~Kulkarni, A.~Singh, G.~Ramakrishnan, and S.~Chakrabarti, \textit{Collective annotation of wikipedia entities in web text}, in \emph{KDD}, 2009, pp. 457--466.

\bibitem{DBLP:series/synthesis/2011Li}
H.~Li, \textit{Learning to Rank for Information Retrieval and Natural Language Processing}, ser. Synthesis Lectures on Human Language Technologies. Morgan {\&} Claypool Publishers, 2011. [Online]. Available:  \url{http://dx.doi.org/10.2200/S00348ED1V01Y201104HLT012}

\bibitem{DBLP:conf/kdd/LiWHHRY13}
Y.~Li, C.~Wang, F.~Han, J.~Han, D.~Roth, and X.~Yan, \textit{Mining evidences for named entity disambiguation}, in \emph{KDD}, 2013, pp. 1070--1078.

\bibitem{Mihalcea07}
R.~Mihalcea and A.~Csomai, \textit{Wikify!: linking documents to encyclopedic knowledge}, in \emph{CIKM}, 2007, pp. 233--242.

\bibitem{DBLP:journals/cacm/Miller95}
G.~A. Miller, \textit{Wordnet: A lexical database for english}, \emph{Commun. ACM}, vol.~38, no.~11, pp. 39--41, 1995.

\bibitem{Milne2008b}
D.~Milne and I.~H. Witten, \textit{An effective, low-cost measure of semantic relatedness obtained from wikipedia links}, in \emph{Proceedings of the AAAI 2008 Workshop on Wikipedia and Artificial Intelligence~(WIKIAI, 2008)}, 2008.

\bibitem{Milne08}
D.~Milne, I. H.~Witten, \textit{Learning to link with wikipedia}, in \emph{CIKM}, 2008, pp. 509--518.

\bibitem{DBLP:conf/acl/PilehvarJN13}
M.~T. Pilehvar, D.~Jurgens, and R.~Navigli, \textit{Align, disambiguate and walk: A unified approach for measuring semantic similarity}, in \emph{ACL}, 2013, pp. 1341--1351.

\bibitem{popov2004kim}
B.~Popov, A.~Kiryakov, D.~Ognyanoff, D.~Manov, and A.~Kirilov, \textit{Kim-a semantic platform for information extraction and retrieval}, \emph{Natural language engineering}, vol.~10, no. 3-4, pp. 375--392, 2004.

\bibitem{Ratinov11}
L.-A. Ratinov, D.~Roth, D.~Downey, and M.~Anderson, \textit{Local and global algorithms for disambiguation to wikipedia}, in \emph{ACL}, 2011, pp.
  1375--1384.

\bibitem{DBLP:journals/ml/ShenJ05}
L.~Shen and A.~K. Joshi, \textit{Ranking and reranking with perceptron}, \emph{Machine Learning}, vol.~60, no. 1-3, pp. 73--96, 2005. [Online].
  Available: \url{http://dx.doi.org/10.1007/s10994-005-0918-9}

\bibitem{Singh2012}
S.~Singh, A.~Subramanya, F.~Pereira, and A.~McCallum, \textit{Wikilinks: A large-scale cross-document coreference corpus labeled via links to   wikipedia}, University of Massachusetts, Tech. Rep. UM-CS-2012-015, 2012.

\bibitem{DBLP:conf/icdm/TongFP06}
H.~Tong, C.~Faloutsos, and J.-Y. Pan, \textit{Fast random walk with restart and its applications}, in \emph{ICDM}, 2006, pp. 613--622.

\bibitem{DBLP:journals/ir/WuBSG10}
Q.~Wu, C.~J.~C. Burges, K.~M. Svore, and J.~Gao, \textit{Adapting boosting for information retrieval measures}, \emph{Inf. Retr.}, vol.~13, no.~3, pp. 254--270, 2010.

\bibitem{DBLP:conf/emnlp/YahyaBERTW12}
M.~Yahya, K.~Berberich, S.~Elbassuoni, M.~Ramanath, V.~Tresp, and G.~Weikum, \textit{Natural language questions for the web of data}, in \emph{Proceedings of the 2012 Joint Conference on Empirical Methods in Natural Language Processing and Computational Natural Language Learning}, 2012, pp. 379--390.

\bibitem{Zhang11}
W.~Zhang, Y.~C. Sim, J.~Su, and C.~L. Tan, \textit{Entity linking with effective acronym expansion, instance selection, and topic modeling}, in \emph{IJCAI}, 2011, pp. 1909--1914.

\bibitem{Zhang10}
W.~Zhang, J.~Su, C.~L. Tan, and W.~Wang, \textit{Entity linking leveraging automatically generated annotation}, in \emph{COLING}, 2010, pp. 1290--1298.

\bibitem{Zheng10}
Z.~Zheng, F.~Li, M.~Huang, and X.~Zhu, \textit{Learning to link entities with knowledge base}, in \emph{HLT-NAACL}, 2010, pp. 483--491.

\bibitem{Zhou10}
Y.~Zhou, L.~Nie, O.~Rouhani-Kalleh, F.~Vasile, and S.~Gaffney, \textit{Resolving surface forms to wikipedia topics}, in \emph{COLING}, 2010, pp. 1335--1343.

\end{thebibliography}

% that's all folks
\end{document}


