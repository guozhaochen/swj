library(plyr)
library(ggplot2)
library(grid)
library(scales)

cbPalette <- c("#8dd3c7", "#fb8072", "#bebada", "#33a02c", "#80b1d3", "#0072B2", "#D55E00", "#CC79A7")
cbPalette <- c("#222222", "#222222", "#222222", "#222222", "#222222", "#222222", "#222222", "#222222")

source("/Users/denilson/work/stup/results/summary.R")
setwd(dir = "/Users/denilson/papers/svn-TKDE-EL/figures/")

clueweb_data <- subset(read.table("AUC_clueweb12_normalized.tsv", header = TRUE), bracket > 0.2)
wikipedia_data <- subset(read.table("AUC_wikipedia_normalized.tsv", header = TRUE), bracket > 0.2)
aida_data <- subset(read.table("AUC_aida_conll_normalized.tsv", header = TRUE), bracket > 0.2)

frames <- list(clueweb_data, wikipedia_data, aida_data)
names <- list("AUC_clueweb12_normalized.eps", "AUC_wikipedia_normalized.eps", "AUC_aida_conll_normalized.eps")

for (i in 1:length(frames)) {
	data <- frames[[i]]
#	data$accuracy = data$accuracy/100
	summary = summarySE(data,measurevar="accuracy",groupvars=c("method"))
	top5 = summary[order(summary$accuracy, decreasing=TRUE)[1:5],]$method
	indexes = apply(data[c("method")], 1, function(row) row %in% top5)
	top5_data = data[indexes,]
	
	top5_data$method <- factor(top5_data$method, top5)

	print(names[[i]])	
	print(summary)
	print(" ")
	

	g <- ggplot(top5_data,aes(x=bracket, y=accuracy, group=method, colour=method, shape=method, linetype=method)) +
	 geom_line(size=0.75) + 
	 geom_point(size=2.5) + 
	 theme_bw() + 
	 scale_colour_manual(values=cbPalette) + 
	 theme(legend.justification=c(1,0), legend.position=c(1,0), legend.key.width = unit(1, "cm")) +
	 stat_function(fun=function(x){x}, geom="line", colour="black", linetype="dotted", size=0.25)
	 
	 print(g)
	 
	 ggsave(names[[i]],width=5,height=3.5)
}
