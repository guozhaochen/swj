library(plyr)
library(ggplot2)
library(grid)
library(scales)

cbPalette <- c("#bebada", "#f5f5f5", "#e41a1c", "#33a02c", "#80b1d3", "#0072B2", "#D55E00", "#CC79A7")

setwd("/Users/denilson/papers/git-wned-tkde/figures/")
source("/Users/denilson/work/stup/results/summary.R")

clueweb_data <- read.table("stats/clueweb_stats.tsv", header = TRUE)
clueweb_data$corpus = "ClueWeb 12"
wikipedia_data <- read.table("stats/wikipedia_stats.tsv", header = TRUE)
wikipedia_data$corpus = "Wikipedia"
aida_conll_data <- read.table("stats/aida_conll_stats.tsv", header = TRUE)
aida_conll_data$corpus = "AIDA CoNLL"



all_data <- rbind(subset(clueweb_data, bracket > 0.2), subset(wikipedia_data, bracket > 0.2), subset(aida_conll_data, 
	bracket > 0.2))

all_data$bracket <- factor(all_data$bracket)

ylegends <- list("mentions per document", "candidates per mention", "average node degree", "number of nodes in graph")
filenames <- list("stats_mentions_per_doc.eps", "stats_candidates_per_mention.eps", "stats_average_degree.eps", "stats_graph_size.eps")
base_charts <- list (ggplot(all_data, aes(x = "0", y = mentionNum, fill = corpus, shape_pal = corpus)), 
ggplot(all_data, aes(x = "0", y = avgCandidateNum, fill = corpus, shape_pal = corpus)), 
ggplot(all_data, aes(x = "0", y = avgDegree, fill = corpus, shape_pal = corpus)),
ggplot(all_data, aes(x = "0", y = nodeNum, fill = corpus, shape_pal = corpus)))


for (i in 1:length(filenames)) {

	g <- base_charts[[i]] + geom_boxplot() + theme_bw() + 
		scale_y_log10(name = "") + 
		facet_grid(. ~ bracket) + theme(legend.position = "top", 
		axis.ticks = element_blank(), axis.text.x = element_blank()) + 
		scale_x_discrete(name = "bracket") +
		scale_fill_manual(values=cbPalette)
		
	print(g)
	ggsave(filenames[[i]],width=5,height=4)
}
